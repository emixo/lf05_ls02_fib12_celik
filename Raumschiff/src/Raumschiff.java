import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int lebenserhaltungsystemInProzent;
	private int androidenNazahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> landungsverzeichnis;
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int lebenserhaltungsystemInProzent, int androidenNazahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
		this.androidenNazahl = androidenNazahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.landungsverzeichnis = new ArrayList<Ladung>();
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getLebenserhaltungsystemInProzent() {
		return lebenserhaltungsystemInProzent;
	}
	public void setLebenserhaltungsystemInProzent(int lebenserhaltungsystemInProzent) {
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
	}

	public int getAndroidenNazahl() {
		return androidenNazahl;
	}
	public void setAndroidenNazahl(int androidenNazahl) {
		this.androidenNazahl = androidenNazahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	 public ArrayList<Ladung> getLandungsverzeichnis() {
			return landungsverzeichnis;
	 }

		public void setLandungsverzeichnis(ArrayList<Ladung> landungsverzeichnis) {
			this.landungsverzeichnis = landungsverzeichnis;
	 }

	@Override
	public String toString() {
		return "Androiden Anzahl = " + androidenNazahl + ", \nBroadcastkommunikator = "
				+ broadcastKommunikator + ", \nEnergieversorgung in Prozent = " + energieversorgungInProzent
				+ ", \nLebenserhaltungsystem in Prozent = "
				+ lebenserhaltungsystemInProzent + ", \nPhotonentorpedo Anzahl = " + photonentorpedoAnzahl
				+  ", \nSchiffsname = " + schiffsname + ", \nSchild in Prozent = "
				+ schildeInProzent;
	}		
}