/**
 * @Author EMIRKAN
 */

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}

	@Override
	public String toString() {
		return "[bezeichnung=" + bezeichnung + ", menge=" + menge + "]";
	}
/// Plasma Waffe 50, Photonentorpedo 3, Klingonen Schwert 200, Ferengi 200, Rote Materie 2, Forschungsonde 35, Schrott 5

Ladung kylo = new Ladung("Plasma Waffe", 50);
Ladung kylon = new Ladung("Photonentorpedo", 3);
Ladung kylos = new Ladung("Klingonen Schwert", 200);
Ladung kylor = new Ladung("Ferengi", 200);
Ladung kylom = new Ladung("Rote Materie", 2);
Ladung kylok = new Ladung("Forschungsonde", 35);
Ladung kylop = new Ladung("Schrott", 5);

}
	

