
public class Main {
	public static void main(String[] args) {
		Raumschiff klingone = new Raumschiff(1, 100, 100, 100, 2, "IKS Heghita");
		System.out.println("\n" + klingone + "\n" );
		
		Raumschiff romulaner = new Raumschiff(2, 50, 100, 100, 2, "IRW Khazara");
		System.out.println(romulaner + "\n" );

		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 5, "NiVar");
		System.out.println(vulkanier + "\n" );
	}
}
